# Vers un système modulaire de publication : éditer avec le numérique

Support de présentation de l'intervention "Vers un système modulaire de publication : éditer avec le numérique" d'[Antoine Fauchié](https://www.quaternum.net/), lors du colloque ["Repenser les humanités numériques / Thinking the Digital Humanities Anew"](https://www.crihn.org/colloque-2018/) organisé par le Centre de recherche interuniversitaire sur les humanités numériques (CRIHN), du 25 au 27 octobre 2018 à Montréal.

Billet présentant cette communication avec les différentes ressources :  
[www.quaternum.net/crihn](https://www.quaternum.net/crihn)

Présentation disponible en ligne :  
[https://presentations.quaternum.net/systeme-modulaire-de-publication-crihn/](https://presentations.quaternum.net/systeme-modulaire-de-publication-crihn/)

Texte de la présentation et bibliographie bientôt sur   
[https://www.quaternum.net/](www.quaternum.net)

## Contexte
Cette intervention est une proposition de résumé du mémoire du même nom, "Vers un système modulaire : éditer avec le numérique", et disponible en ligne : [https://memoire.quaternum.net](https://memoire.quaternum.net).

## Plan de l'intervention

0. Préambule : démarche singulière, présentation d'un mémoire de Master ;
1. Éditer avec le numérique : influence du numérique sur le livre, distinction éditer/publier ;
2. Modularité : le principe de plusieurs chaînes de publication, le centre de trois principes ;
3. Système : passage d'une chaîne linéaire à une synergie dynamique ;
4. Apprentissage : des pratiques à adopter (en humanités numériques).

## Proposition originale
Voici la proposition originale transmise au comité organisateur du colloque en août 2018 :

Le domaine du livre, et plus particulièrement l'édition, connaît des mutations profondes au contact du numérique (Benhamou, 2014).
L'une des manifestations les plus visibles de cette rencontre entre l'imprimé et le numérique reste probablement le livre numérique, constatant toutefois que les hybridations sont à la fois nombreuses et en devenir (Ludovico, 2016).
Face aux bouleversements économiques et aux nouveaux modes d'accès aux textes, des changements plus profonds s'opèrent en creux (Epron et Vitali Rosati, 2018).
Si l'ebook est une nouvelle approche de l'écrit tant dans sa diffusion que dans sa réception, n'y a-t-il pas en filigrane des transformations plus fondamentales dans la manière de faire des livres ?
Depuis l'avènement de l'informatisation des métiers du livre, ou l'omniprésence des traitements de texte (Dehut, 2018) et des logiciels de publication assistée par ordinateur (Masure, 2011), les façons de concevoir et de produire des publications connaissent une révolution technologique récente (Eberle-Sinatra et Vitali Rosati, 2014).
Des structures d'édition imaginent des nouvelles chaînes de publication originales et non conventionnelles.
Celles-ci ne reposent plus sur un solutionnisme technologique – un problème a forcément une solution logicielle ou applicative (Morozov, 2014) –, elles repositionnent l'humain au cœur des machines ou des programmes (Simondon, 2012), et envisagent la publication comme un ensemble d'actions imbriquées.
Les méthodes et les technologies issues du développement web influencent la manière de faire des livres, et permettent de considérer un système modulaire et non plus une chaîne linéaire (Fauchié et Parisot, 2018).
Nous nous proposons d'analyser deux exemples de chaînes de publication non classiques dans les domaines de l'édition d'art (Evans Lan, 2016) et de l'édition scientifique (Rosenthal, 2017), et de leur donner des perspectives théoriques dans le champ de la philosophie de la technique.
À partir de cet examen nous souhaitons avancer les trois principes d'un nouveau modèle de publication : interopérabilité, modularité et multiformité.
Un système interopérable, modulaire et multiforme pourrait être en mesure de répondre à différentes contraintes ou exigences de publication.
Tout d'abord l'interconnexion des outils qui composent une chaîne d'édition impose une interopérabilité, celle-ci peut par exemple se baser sur l'utilisation de langages de balisage léger plutôt que sur des formats fermés et liés à des logiciels spécifiques.
La structure étant séparée de la mise en forme – une modification des contenus ne doit pas avoir d'incidence sur le rendu graphique de ceux-ci –, il faut considérer la dimension modulaire du système : chaque composant peut être interchangeable pour permettre une adaptabilité.
Enfin, ce système doit pouvoir générer différentes formes d'une même publication : édition imprimée – offset ou impression à la demande –, versions numériques – web ou EPUB –, jeux de données.
Nous souhaitons également exposer les limites d'une telle approche, tant dans son fonctionnement que dans son adhésion : la dépendance à des logiciels fermés se déplace vers des développements qui peuvent créer une dette technique conséquente (Jaillot, 2015) ; la facilité d'accès d'un tel système requiert la création d'interfaces utilisateurs accessibles (Garnder, 2017) ; l'hybridation de l'imprimé et du numérique réserve sans doute de nouvelles formes que nous ne pouvons pas prévoir actuellement (Ludovico, 2016).

Bibliographie originale :

Benhamou, F. (2014). Le livre à l’heure numérique: papiers, écrans, vers un nouveau vagabondage. Paris, France: Éditions du Seuil.  
Dehut, J. (s. d.). En finir avec Word ! Pour une analyse des enjeux relatifs aux traitements de texte et à leur utilisation [Billet]. Consulté le 29 mars 2018, à l’adresse https://eriac.hypotheses.org/80  
Eberle-Sinatra, M., & Vitali Rosati, M. (Éd.). (2014). Pratiques de l’édition numérique. Montréal, Canada: Presses de l’Université de Montréal, DL 2014.  
Epron, B., & Vitali-Rosati, M. (2018). L’édition à l’ère numérique. Paris: La Découverte. Consulté le à l’adresse https://www.cairn.info/l-edition-a-l-ere-numerique--9782707199355.htm  
Evan Lan, R. (2016, mai 16). An Editor’s View of Digital Publishing. Consulté le 30 avril 2018, à l’adresse http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/  
Fauchié, A. (2017, janvier 23). Publier des livres avec un générateur de site statique. Consulté le 30 avril 2018, à l’adresse https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/  
Jaillot, B. (2015). La dette technique. Périgneux, France: le Train de 13h37.  
Masure, A. (2011). Adobe Le créatif au pouvoir ? Strabic. Consulté le à l’adresse  http://strabic.fr/Adobe-le-creatif-au-pouvoir  
Morozov, E. (2014). Pour tout résoudre, cliquez ici: l’aberration du solutionnisme technologique. (M.-C. Braud, Trad.). Limoges, France: Fyp, impr. 2014.  
Rosenthal, D. (2017, mai 2). Distill: Is This What Journals Should Look Like? Consulté le 1 juin 2018, à l’adresse https://blog.dshr.org/2017/05/distill-is-this-what-journals-should.html  
Simondon, G. (2012). Du mode d’existence des objets techniques. Paris, France: Aubier, impr. 2012.
